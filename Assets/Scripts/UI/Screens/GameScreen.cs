using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����� ������ ����. 
/// ����� ������������ ����� UI-������, �������������� ������ ���� ���� 
/// ��� ������������� ������-���� �������� �������. 
/// ��� ����� ���� ����� �������� ����, ����� � ����������� � ���������� ������, ����� Game over, � �.�.)
/// ������ ������ ������ ���� �������� �� UI-������ ������.
/// </summary>
public abstract class GameScreen : MonoBehaviour
{
    /// <summary>
    /// ��������/������ �����
    /// </summary>
    /// <param name="active"></param>
    public virtual void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }
}
