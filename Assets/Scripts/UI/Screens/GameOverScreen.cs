using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// �����, ������������ ��� ���������.
/// ������ ������� ��������� �� UI-������, ������� ������ ���������� ��� ���������.
/// </summary>
public class GameOverScreen : GameScreen
{
    [SerializeField]
    [Tooltip("������ �������� ������")]
    private Button restartButton = null;

    [SerializeField]
    [Tooltip("������ ������ �� ����")]
    private Button quitButton = null;

    private void Start()
    {
        restartButton.onClick.AddListener(() => GameManager.Instance.Controller.RunFromLastCheckPoint());
        quitButton.onClick.AddListener(() => GameManager.Instance.Controller.Quit());
    }
}
