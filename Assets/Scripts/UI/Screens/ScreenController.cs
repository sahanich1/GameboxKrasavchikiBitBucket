using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ���������� ������� ������� (������).
/// ���� ������ UI-������� ����, ���������� � �������� ��.
/// ����������� ��������� �������� �������� � GameManager.
/// </summary>
public class ScreenController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("����� ��������� ����")]
    private GameScreen gameOverScreen = null;

    /// <summary>
    /// ������ ���� �������
    /// </summary>
    private readonly List<GameScreen> screens = new List<GameScreen>();

    /// <summary>
    /// ������ ������� �����
    /// </summary>
    public void HideCurrentScreen()
    {
        SetActiveScreen(null);
    }

    /// <summary>
    /// �������� ����� ��������� ����
    /// </summary>
    public void ShowGameOverScreen()
    {
        SetActiveScreen(gameOverScreen);
    }

    private void Awake()
    {
        screens.Add(gameOverScreen);
        SetActiveScreen(null);
    }

    /// <summary>
    /// �������� ��������� �����. ������������ ����� ���� ������� ������ ���� �����,
    /// ������� ��� ��������� ����� ������.
    /// </summary>
    /// <param name="screen"></param>
    private void SetActiveScreen(GameScreen screen)
    {
        foreach (var item in screens)
        {
            item.SetActive(item == screen);
        }        
    }

}
