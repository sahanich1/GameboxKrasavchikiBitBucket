using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// ��������� ��������, ����������� ��� �������������� ��
/// </summary>
public class AIPatrolStrategy : AIMovementStrategy
{
    /// <summary>
    /// ����������, ������� ����� �� ����� �� ����� ��������, ����� ����� ������ ������ ��������
    /// � ���������
    /// </summary>    
    private const float MinDistanceToSmoothCorner = 0.25f;
    
    /// <summary>
    /// ������� ���������� (��� �����������)
    /// </summary>
    private const float MinSqrDistanceToSmoothCorner = MinDistanceToSmoothCorner * MinDistanceToSmoothCorner;

    /// <summary>
    /// �����, �������� ��������� ������� ����� �������� � ���������� �� ����� ���������
    /// </summary>
    private class CurrentPathParam
    {
        /// <summary>
        /// ������� ��������
        /// </summary>
        private NPCPath path;
        /// <summary>
        /// ������ ������� ����� ��������
        /// </summary>
        private int pointIndex;
        /// <summary>
        /// ����������� �������� (1 - �������� � ������ �����������, -1 - � ��������)
        /// </summary>
        private int directionIndexIncrement;

        /// <summary>
        /// ������ ������� ����� ��������
        /// </summary>
        public int PointIndex => pointIndex;

        /// <summary>
        /// �����������, ������� ��������� ������� �������� � ������� ����� ��, � ������������
        /// ����� ��������, ��������� � ������� �����
        /// </summary>
        /// <param name="path">������� ��������</param>
        /// <param name="currentPosition">������� �����, �� ������� ��������� ������ ��</param>
        public CurrentPathParam(NPCPath path, Vector3 currentPosition)
        {
            this.path = path;
            pointIndex = path.GetNearestPoint(currentPosition);
            directionIndexIncrement = 1;
        }

        /// <summary>
        /// ������� ��������� ����� �������� ��� ��������
        /// </summary>
        public void SetNextPoint()
        {
            int nextPointIndex = GetNextPointInDirection(directionIndexIncrement);

            if (!path.IsClosed && nextPointIndex > pointIndex && directionIndexIncrement < 0 
                || nextPointIndex < pointIndex && directionIndexIncrement > 0)
            {
                directionIndexIncrement = -directionIndexIncrement;
            }

            pointIndex = nextPointIndex;
        }

        public int GetNextPointInDirection(int direction)
        {
            return direction > 0 ? path.GetNextPoint(pointIndex) 
                : path.GetPreviousPoint(pointIndex);
        }
    }

    [SerializeField]
    [Tooltip("������ �������� ��������")]
    private PathData pathData;

    /// <summary>
    /// ��������� ������� �� ����� ������� �������������� 
    /// </summary>
    private Vector3 initialDirection;

    /// <summary>
    /// ������ �������� ��������
    /// </summary>
    public PathData PathData => pathData;

    /// <summary>
    /// ���������������� ������ ��������� ��������
    /// (������ ����� ���������� �������� ����� ������� StartMoving)
    /// </summary>
    /// <param name="movingObject">���������� ������ ��</param>
    public void Initialize(AIMovingObject movingObject)
    {
        BaseInitialize(movingObject);
        initialDirection = movingObject.transform.forward;
    }

    /// <summary>
    /// ��������, ������� ��������� �������� �� �������� ��������������
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        movingObject.SetEnabledAutomaticRotation(true);

        Coroutine rotatingCoroutine;

        // ���� ������� ����� ������ ���� �����, �� ��� ��������, ��� �� ��� ����������� ������
        // ����������� �������, ��� �������� � ��������� ������
        bool infiniteRotation = pathData.Path.PointsCount <= 1;

        CurrentPathParam currentPathParam = new CurrentPathParam(pathData.Path, movingObject.Position);

        int pointIndex = currentPathParam.PointIndex;
        // ������� ��������� �������� ������� �� �� ������� ����� � ��������� ����� ��������
        movingObject.MoveToPoint(pathData.Path[pointIndex]);
        yield return new WaitForFixedUpdate();
        while (!movingObject.IsDestinationReached())
        {
            yield return new WaitForFixedUpdate();
        }

        // ��� ������ ��������� �� �������, ��������� �������������� ������� �������.
        // ����� �������������� ������������ ������ �� � ��������������� �����������.
        movingObject.SetEnabledAutomaticRotation(false);

        while (true)
        {
            pointIndex = currentPathParam.PointIndex;
            movingObject.MoveToPoint(pathData.Path[pointIndex]);

            Vector3 pointToLookAt = pathData.Path[pointIndex] + GetSightDirectionInPoint(pointIndex);
            rotatingCoroutine = StartCoroutine(PerformInfiniteLookAt(pointToLookAt, movingObject.RotationSpeed));           

            PathPointData pointData = pathData.GetPointData(currentPathParam.PointIndex);

            yield return new WaitForFixedUpdate();
            while (!movingObject.IsDestinationReached())
            {
                yield return new WaitForFixedUpdate();

                // ���� �� ��������� ����� �������� �������� ������ �������, � �� ��� �� ����� ������,
                // �� �� ����� ��������� � ���������. ����� ������� �� ���������� ����������� �� ��������,
                // ����� �� ���� ���������� � ��������� �� ����� ������.
                if (pointData.StopTime == 0 &&
                    movingObject.GetSqrDistanceToPoint(pathData.Path[pointIndex]) < MinSqrDistanceToSmoothCorner)
                {
                    break;
                }
            }

            this.StopAndNullCoroutine(ref rotatingCoroutine);

            // ������������ �������, �� ������� ������ ���� �������� ������ �� � ������ �����
            if (infiniteRotation || pointData.StopTime > 0)
            {
                float angleToSightDirectionInPoint = VectorFunc.GetSignedAngleXZ(movingObject.transform.forward,
                    GetSightDirectionInPoint(pointIndex));
                float rotationTime = Mathf.Abs(angleToSightDirectionInPoint / movingObject.RotationSpeed);
                rotatingCoroutine = StartCoroutine(PerformRotating(angleToSightDirectionInPoint, movingObject.RotationSpeed));
                float endTime = Time.time + rotationTime;
                while (Time.time < endTime)
                {
                    yield return new WaitForFixedUpdate();
                }
                this.StopAndNullCoroutine(ref rotatingCoroutine);

                // ���� �� ����� ����� ���������� ������, �� �������������, ������ ������� �� �������� 
                if (infiniteRotation && pointData.RotationAngle == 0)
                {
                    break;
                }

                // ��������� �����, ����� ����� ����� ���������� �������� �� ��������
                if (pointData.RotationAngle > 0)
                {
                    float timeToNextMove = Time.time + pointData.StopTime;

                    // ��������� �������� �����-������ ��������� ��� ����� �����
                    rotatingCoroutine = StartCoroutine(
                        PerformInfiniteRotatingLeftRight(pointData.RotationAngle, movingObject.FovRotationSpeed));
                    while (infiniteRotation || Time.time < timeToNextMove)
                    {
                        yield return new WaitForFixedUpdate();
                    }
                    this.StopAndNullCoroutine(ref rotatingCoroutine);
                }
            }

            // ������������ ��������� ����� �������� �� ��������
            currentPathParam.SetNextPoint();
        }

        movingCoroutine = null;
    }

    /// <summary>
    /// �������� ����������� ������� � ����� ��������������
    /// </summary>
    /// <param name="pointIndex">������ ����� ��������������, 
    /// ��� ������� ����� ���������� ����������� �������</param>
    /// <returns></returns>
    private Vector3 GetSightDirectionInPoint(int pointIndex)
    {
        // ���� ����� ����, ���������� ����������� ������� ��, ���������� �������� �� �����
        if (pathData.Path.PointsCount <= 1)
        {
            return initialDirection;
        }

        // ������� ��� �����������: �� ���������� ����� �������� � ��������� � �� ��������� ����� �������� � ���������
        // ������, ���������� ������������� ���� ����������� � ����� ����� �� ����������� �������
        int nextPointIndex = pathData.Path.GetNextPoint(pointIndex);
        int prevPointIndex = pathData.Path.GetPreviousPoint(pointIndex);
        Vector3 direction1 = (pathData.Path[pointIndex] - pathData.Path[nextPointIndex]).normalized;
        Vector3 direction2 = (pathData.Path[pointIndex] - pathData.Path[prevPointIndex]).normalized;

        return (direction1 + direction2).normalized;
    }
}
