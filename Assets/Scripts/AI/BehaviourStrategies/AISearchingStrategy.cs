using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ��������� ��������, ����������� ��� ������ �������� �� ���� (������), 
/// ������� ����� �� ���� ������
/// </summary>
public class AISearchingStrategy : AIMovementStrategy
{
    /// <summary>
    /// ���� ��������, ������� ����� ��������� �� �������� �� �����, 
    /// �� ������� ���� � ��������� ��� �������� ����
    /// </summary>
    private const float RotationAngle = 360;

    /// <summary>
    /// ����� ����� ��������, ������� ���� ��� � �����, �� ������� ���� � ��������� 
    /// ��� �������� ����, ����� ���, ��� ����� ��������� � ��������������
    /// </summary>
    private const float DelayBeforeReturnToPatrol = 0.5f;

    /// <summary>
    /// ����, ������� ����� ������
    /// </summary>
    private Transform target;

    /// <summary>
    /// ���������������� ������ ��������� ��������
    /// (������ ����� ���������� �������� ����� ������� StartMoving)
    /// </summary>
    /// <param name="movingObject">���������� ������ ��</param>
    /// <param name="target">����</param>
    public void Initialize(AIMovingObject movingObject, Transform target)
    {
        BaseInitialize(movingObject);
        this.target = target;
    }

    /// <summary>
    /// ��������, ������� ��������� ����� ����
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        // ���� ����� ��������� �������� �� �����, ��� � ��������� ��� ��� ������� �����.
        // ���� ��������� �������� �� ����������� �������� � ���� �����, ����� �������� �������� ���� ������,
        // ����� �������� � ��� �����������
        Vector3 pointToLookAt = target.position + (target.position - movingObject.Position).normalized;
        float angleToLastTargetPoint = VectorFunc.GetSignedAngleXZ(movingObject.transform.forward,
            pointToLookAt - movingObject.Position);

        movingObject.SetEnabledAutomaticRotation(false);
        Coroutine rotatingCoroutine = StartCoroutine(PerformInfiniteLookAt(pointToLookAt, 
            movingObject.AlertRotationSpeed));

        // �������� � �����, �� ������� � ��������� ��� ��� ������� �����
        movingObject.MoveToPoint(target.position);

        yield return new WaitForFixedUpdate();

        while (!movingObject.IsDestinationReached())
        {
            yield return new WaitForFixedUpdate();
        }
        this.StopAndNullCoroutine(ref rotatingCoroutine);      

        // ����� ����, ��� ����� �� �����, ���� ��������� �������, ����� ���������,
        // ��� ���� ��� ������ ����. ������� ��������� � �����������, � ������� ������� �����
        float rotationAngle = angleToLastTargetPoint >= 0 ? RotationAngle : -RotationAngle;

        float rotationSpeed = movingObject.RotationSpeed;
        float fullRotationTime = Mathf.Abs(rotationAngle) / rotationSpeed;

        rotatingCoroutine = StartCoroutine(PerformRotating(rotationAngle, rotationSpeed));
        float endTime = Time.time + fullRotationTime;
        while (Time.time < endTime)
        {
            yield return new WaitForFixedUpdate();
        }

        this.StopAndNullCoroutine(ref rotatingCoroutine);

        // ����� ��������� �������� ����� ������������ � ��������������
        yield return new WaitForSeconds(DelayBeforeReturnToPatrol);

        movingCoroutine = null;
    }
}
