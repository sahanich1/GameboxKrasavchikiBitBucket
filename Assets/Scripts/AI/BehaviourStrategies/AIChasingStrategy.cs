using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ��������� ��������, ����������� ��� ������ �� �� �����
/// </summary>
public class AIChasingStrategy : AIMovementStrategy
{
    /// <summary>
    /// ������ �������� ��������� ���� (�������� ����� ����������� ��������� ��������)
    /// </summary>
    const float CheckPeriod = 0.1f;

    /// <summary>
    /// ����, �� ������� �������
    /// </summary>
    private Transform target;

    /// <summary>
    /// ���������������� ������ ��������� ��������
    /// (������ ����� ���������� �������� ����� ������� StartMoving)
    /// </summary>
    /// <param name="movingObject">���������� ������ ��</param>
    /// <param name="target">���� ������</param>
    public void Initialize(AIMovingObject movingObject, Transform target)
    {
        BaseInitialize(movingObject);
        this.target = target;
    }

    /// <summary>
    /// ��������, ������� ��������� �������� � ������� �� ����������� � ����
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        // ��������� �������������� ������� NavMeshAgent, �.�. ������ �� ����� ������ ������� � ����,
        // ���������� �� ����������� ��������
        movingObject.SetEnabledAutomaticRotation(false);
        Coroutine rotatingCoroutine = StartCoroutine(PerformInfiniteLookAt(target,
            movingObject.AttackRotationSpeed));
        while (target != null)
        {
            movingObject.MoveToPoint(target.position);
            yield return new WaitForSeconds(CheckPeriod);
        }

        this.StopAndNullCoroutine(ref rotatingCoroutine);
        movingCoroutine = null;
    }
}
