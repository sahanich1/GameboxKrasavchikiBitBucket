using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-������� ������ �����
/// </summary>
public class IsDead : FsmCondition
{
    /// <summary>
    /// ������ �������� �����
    /// </summary>
    private IDamageable healthObject;

    /// <summary>
    /// ���������, ��������������� �� �������
    /// </summary>
    /// <param name="curr"></param>
    /// <param name="next"></param>
    /// <returns></returns>
    public override bool IsSatisfied(FsmState curr, FsmState next)
    {
        return !healthObject.IsAlive();
    }

    private void Awake()
    {
        healthObject = GetComponentInParent<IDamageable>();
    }
}
