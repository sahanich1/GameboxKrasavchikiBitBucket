using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-��������� �������� ������ ������ ������.
/// ���������, � ������� ��������� ����, ����� ������� ������ �� ����.
/// �������, ����� �� ������� ������������� ��������� AISearchingStrategy (��� ����������
/// ������������ ��������� ������).
/// ��� ��������� ������ ���������� � �������� �������� �������-�����, 
/// �.�. �� ��������� ����������� ���������� �� ���
/// </summary>
[RequireComponent(typeof(AISearchingStrategy))]
public class SearchingState : FsmState
{
    /// <summary>
    /// ���������� ������ ��
    /// </summary>
    private AIMovingObject movingObject;

    /// <summary>
    /// ��������� ������
    /// </summary>
    private AISearchingStrategy searchingStrategy;

    /// <summary>
    /// �����, ���������� ��� �������� � ���������
    /// </summary>
    public override void OnStateEnter()
    {
        movingObject.SetAttackSpeed();

        searchingStrategy.Initialize(movingObject, GameManager.Instance.Player.transform);
        searchingStrategy.StartMoving();
    }

    /// <summary>
    /// �����, ���������� ��� ������ �� ���������
    /// </summary>
    public override void OnStateLeave()
    {
        searchingStrategy.StopMoving();
    }

    /// <summary>
    /// ����������� �� �����. ���������� true, ���� ������� ������ ��� �� ��������,
    /// false - ���� ��� �������� �� ������ ���� ���������
    /// </summary>
    /// <returns></returns>
    public override bool IsPerforming()
    {       
        return !searchingStrategy.IsStopped();
    }

    private void Awake()
    {
        movingObject = GetComponentInParent<AIMovingObject>();
        searchingStrategy = GetComponent<AISearchingStrategy>();       
    }
}
