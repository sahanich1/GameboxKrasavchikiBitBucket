using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-��������� �������������� �����.
/// ��������� ����� �� ���������. 
/// ����� ���� ��������� � ����, ���� ������������ ������� ������ �� ����.
/// �������, ����� �� ������� ������������� ��������� AIPatrolStrategy (��� ����������
/// �������������� �� ��������).
/// ��� ��������� ������ ���������� � �������� �������� �������-�����, 
/// �.�. �� ��������� ����������� ���������� �� ���
/// </summary>
[RequireComponent(typeof(AIPatrolStrategy))]
public class PatrolState : FsmState
{
    /// <summary>
    /// ��������� ��������������
    /// </summary>
    private AIPatrolStrategy patrolStrategy;

    /// <summary>
    /// ���������� ������ ��
    /// </summary>
    private AIMovingObject movingObject;

    /// <summary>
    /// ���� ������ �����
    /// </summary>
    private FieldOfView fov;

    /// <summary>
    /// �����, ���������� ��� �������� � ���������
    /// </summary>
    public override void OnStateEnter()
    {
        fov.SetIdleDetectionDelay();
        movingObject.SetIdleSpeed();

        patrolStrategy.StartMoving();
    }

    /// <summary>
    /// �����, ���������� ��� ������ �� ���������
    /// </summary>
    public override void OnStateLeave()
    {
        patrolStrategy.StopMoving();
    }

    private void Awake()
    {
        movingObject = GetComponentInParent<AIMovingObject>();
        fov = GetComponentInParent<FieldOfView>();

        patrolStrategy = GetComponent<AIPatrolStrategy>();
        patrolStrategy.Initialize(movingObject);
    }
}
