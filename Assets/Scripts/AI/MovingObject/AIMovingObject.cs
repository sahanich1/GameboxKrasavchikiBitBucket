using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// �����, ���������� �� �������� �������, ������������ ��� ����������� ��
/// ����������� �� ������� ������ �����.
/// </summary>
public class AIMovingObject : MovingObject, ISaveable
{
    /// <summary>
    /// ����������� �������� ��������� �� ������� ����� ���������� �� �������� �����,
    /// ����� ����� ����� �������� ����� ����������
    /// </summary>
    private const float MinSqrDestinationDifferenceToMove = 0.1f;

    [SerializeField]
    [Tooltip("�������� �������� � ������ ���")]
    protected float attackSpeed = 5;

    [SerializeField]
    [Tooltip("�������� �������� � ������ ���")]
    private float attackRotationSpeed = 180;

    [SerializeField]
    [Tooltip("�������� �������� � ��������� �������")]
    private float alertRotationSpeed = 120;

    [SerializeField]
    [Tooltip("�������� �������� ���� ������ (������)")]
    private float fovRotationSpeed = 20;
    
    [SerializeField]
    [Tooltip("��������� NavMesh ������, ������� ��������� �� ��������� �� NavMesh")]
    private NavMeshAgent navMeshAgent = null;

    /// <summary>
    /// ��������, ����������� �������� � �������� �����
    /// </summary>
    private Coroutine movingCoroutine = null;

    /// <summary>
    /// ������� �������� ������� ��
    /// </summary>
    public override Vector3 CurrentVelocity => navMeshAgent.velocity;

    /// <summary>
    /// ������� �������� ��������, ������� �������� ������� ������
    /// </summary>
    public override float CurrentDesiredSpeed => navMeshAgent.speed;

    /// <summary>
    /// �������� �������� � ������ ���
    /// </summary>
    public float AttackRotationSpeed => attackRotationSpeed;

    /// <summary>
    /// �������� �������� � ��������� �������
    /// </summary>
    public float AlertRotationSpeed => alertRotationSpeed;

    /// <summary>
    /// �������� �������� ���� ������
    /// </summary>
    public float FovRotationSpeed => fovRotationSpeed;

    /// <summary>
    /// ���������� ������� �������� ��������
    /// </summary>
    public void SetIdleSpeed()
    {
        navMeshAgent.speed = speed;
    }

    /// <summary>
    /// ���������� �������� �������� � ��������� �����
    /// </summary>
    public void SetAttackSpeed()
    {
        navMeshAgent.speed = attackSpeed;
    }

    /// <summary>
    /// ��������� �������� � �������� �����
    /// </summary>
    /// <param name="point"></param>
    public override void MoveToPoint(Vector3 point)
    {
        // ���� �������� ������ �� �������� � ����� �����, �� ��������, �� ��������� ��
        // ������ ��� � ��������
        if (movingCoroutine != null)         
        {
            // �� �������� ����� ����, ���� ������� ����� ���������� ������ ���������� � ����� ����
            if (GetSqrDistanceToPoint(point) < MinSqrDestinationDifferenceToMove)
            {
                return;
            }
            // ��������� ������� ��������
            Stop();
        }
        
        // ���������� �������� � ����� �����
        movingCoroutine = StartCoroutine(PerformMovingToPoint(point));
    }

    /// <summary>
    /// ��������� ������������� � ��������� �����
    /// </summary>
    /// <param name="point"></param>
    public void JumpToPoint(Vector3 point)
    {
        if (!IsAINavigationEnabled())
        {
            return;
        }
        navMeshAgent.Warp(point);
    }

    /// <summary>
    /// ���������� ����������� ��������
    /// </summary>
    public void Stop()
    {
        if (!IsAINavigationEnabled())
        {
            return;
        }
        navMeshAgent.destination = Position;
        this.StopAndNullCoroutine(ref movingCoroutine);
    }

    /// <summary>
    /// ��������/��������� �������������� ������� ������� �� �� ������� ��������
    /// </summary>
    /// <param name="enabled">true - ��������, false - ���������</param>
    public void SetEnabledAutomaticRotation(bool enabled)
    {
        navMeshAgent.updateRotation = enabled;
    }

    /// <summary>
    /// ���������, ��������� �� ������� ��������
    /// </summary>
    /// <returns></returns>
    public bool IsDestinationReached()
    {
        return movingCoroutine == null;
    }

    /// <summary>
    /// ��������/��������� ������ ��������.
    /// ����� ������������, ���� ����� ��������� ����������� ��������, � ����� ������ �����
    /// </summary>
    /// <param name="enabled">true - ��������, false - ���������</param>
    public void SetEnabled(bool enabled)
    {
        if (navMeshAgent.enabled != enabled)
        {
            navMeshAgent.enabled = enabled;
        }        
    }

    /// <summary>
    /// �������� �� ��������� ��
    /// </summary>
    /// <returns></returns>
    public bool IsAINavigationEnabled()
    {
        return navMeshAgent.enabled;
    }

    /// <summary>
    /// ���������� ������� �� ����� �� NavMesh
    /// </summary>
    /// <param name="point">�����, �� ������� �������������� �������</param>
    /// <param name="pathPoints">����� ��������</param>
    /// <returns>true - ����� ��������� � ������� ��� ��������, false - ����� �� ���������</returns>
    public bool CalculatePathToPoint(Vector3 point, out Vector3[] pathPoints)
    {
        pathPoints = null;
        if (!IsAINavigationEnabled())
        {
            return false;
        }
        
        NavMeshPath path = new NavMeshPath();       
        if (navMeshAgent.CalculatePath(point, path) && path.status == NavMeshPathStatus.PathComplete)
        {
            pathPoints = path.corners;
            return true;
        }

        return false;
    }

    /// <summary>
    /// ������ ��������� �������. 
    /// ������������ ��� �������������� ��������� ������� ��� ����������� �� ����������� �����.
    /// ������������ ����� ����� ���� ����������� ��� �������� �� �����,
    /// �� ������ �� ���������� ��� ������ ��� ����, ����� ��� ������ ���� � ����������� �����
    /// ������������ �������������� ��������� �������
    /// </summary>
    /// <param name="state"></param>
    public override void SetState(object state)
    {
        SetEnabled(false);
        base.SetState(state);
        SetEnabled(true);
    }

    protected override void Awake()
    {
        base.Awake();
        MaxSpeed = attackSpeed;

        navMeshAgent.speed = speed;
        navMeshAgent.angularSpeed = rotationSpeed;  
    }

    /// <summary>
    /// ���������, ���� �� ���������� �� ����� ����������, �������� � navMeshAgent.destination
    /// </summary>
    /// <returns></returns>
    private bool IsNavMeshDestinationReached()
    {
        if (!IsAINavigationEnabled())
        {
            return false;
        }

        if (!navMeshAgent.pathPending)
        {
            if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// ��������, ����������� �������� � �������� �����
    /// </summary>
    /// <param name="finishTargetPoint">�������� �����</param>
    /// <returns></returns>
    private IEnumerator PerformMovingToPoint(Vector3 finishTargetPoint)
    {
        // ���� ��������� �� ���������, �������� � �������� ����� ����� �������� �� ��� ���, ����
        // ��� ����� �� ����� ��������
        while (!IsAINavigationEnabled())
        {
            yield return new WaitForFixedUpdate();
        }

        navMeshAgent.destination = finishTargetPoint;

        while (!IsNavMeshDestinationReached())
        {
            yield return new WaitForFixedUpdate();
        }

        movingCoroutine = null;
    }

}
