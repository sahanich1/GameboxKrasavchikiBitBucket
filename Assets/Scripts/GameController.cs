using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// ����������, ����������� �������� ������������ ����
/// �������� �� ������� ���� �� ���������� ������� (��������, ����� ���������������� ������ ��� ������ ������).
/// </summary>
public class GameController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("���������� ������� �������")]
    private ScreenController screenController = null;

    [SerializeField]
    [Tooltip("���������� ����������")]
    private CheckPointController checkPointController = null;

    [SerializeField]
    [Tooltip("���������� ��������� ���������� ��������")]
    private OpacityController opacityController = null;

    public OpacityController OpacityController => opacityController;

    /// <summary>
    /// ������ HP ������
    /// </summary>
    private IDamageable playerHealth = null;

    /// <summary>
    /// ����� �� ����
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }

    /// <summary>
    /// ���������/������������� ������� ����� ����
    /// </summary>
    public void RunGame()
    {
        SceneManager.LoadScene(GameManager.Instance.GameSceneName);
    }

    /// <summary>
    /// ������������� ���� � ��������� ����������� �����
    /// </summary>
    public void RunFromLastCheckPoint()
    {
        opacityController.SetEnabled(false);
        
        screenController.HideCurrentScreen();
        checkPointController.LoadLastCheckPointState();

        opacityController.SetDefaultState();
        opacityController.SetEnabled(true);
    }

    private void Start()
    {
        playerHealth = GameManager.Instance.Player;
        playerHealth.RegisterKilledListener(OnPlayerKilled);
    }

    private void OnDestroy()
    {
        if (playerHealth != null)
        {
            playerHealth.UnregisterKilledListener(OnPlayerKilled);
        }
    }

    /// <summary>
    /// �������, ���������� ��� ������ ������
    /// </summary>
    private void OnPlayerKilled()
    {
        screenController.ShowGameOverScreen();
    }
}
