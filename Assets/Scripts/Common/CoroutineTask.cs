﻿/// <summary>
/// Класс задачи корутины.
/// Объект данного класса представляет собой токен, который передаётся в корутину.
/// Применяется для осуществления контроля выполнения корутины и управлением ей извне.
/// Например, можно дождаться окончания выполнения корутины ( выполнить ожидание пока 
/// флаг IsPerforming не будет выставлен в false. 
/// Также можно прервать корутину, вызвав у токена метод Cancel извне.
/// </summary>
public class CoroutineTask
{
    /// <summary>
    /// Выполняется ли корутина
    /// </summary>
    public bool IsPerforming { get; private set; } = false;

    /// <summary>
    /// Запрошено ли прерывание корутины
    /// </summary>
    public bool IsCancellationRequested { get; private set; } = false;

    /// <summary>
    /// Начать задачу корутины
    /// </summary>
    public void Start()
    {
        IsPerforming = true;
        IsCancellationRequested = false;
    }

    /// <summary>
    /// Остановить задачу корутины
    /// </summary>
    public void Stop()
    {
        IsPerforming = false;
        IsCancellationRequested = false;
    }

    /// <summary>
    /// Запросить отмену выполнения корутины
    /// </summary>
    public void Cancel()
    {
        IsCancellationRequested = true;
    }
}
