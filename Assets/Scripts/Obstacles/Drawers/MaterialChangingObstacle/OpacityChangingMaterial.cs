﻿using UnityEngine;

/// <summary>
/// Материал, изменяющий уровень своей видимости.
/// </summary>
public class OpacityChangingMaterial
{
    /// <summary>
    /// Исходный материал
    /// </summary>
    public Material InitialMaterial { get; private set; }

    /// <summary>
    /// Материал, у которого может изменяться видимость
    /// </summary>
    public Material ChangingMaterial { get; private set; }

    /// <summary>
    /// Алгоритм изменения материала
    /// </summary>
    public MaterialChangingStrategy OpacityChangingStrategy { get; private set; }

    /// <summary>
    /// Инвертирован ли процесс изменения прозрачности
    /// </summary>
    public bool InvertedChange { get; private set; }

    public OpacityChangingMaterial(Material material,
        MaterialChangingStrategy opacityChangingParameters, bool invertedChange)
    {
        InitialMaterial = material;
        // Создадим копию материала, с которой будем работать. Этой копией необходимо заменить
        // исходный материал рендерера.
        // Работаем с копией, т.к. если используется sharedMaterials, то их изменение приведёт
        // к изменению материалов в ассетах.
        ChangingMaterial = new Material(material);
        OpacityChangingStrategy = opacityChangingParameters;
        InvertedChange = invertedChange;
    }

    /// <summary>
    /// Задать уровень видимости материала
    /// </summary>
    /// <param name="opacityValueInPercents">Уровень видимости в процентах</param>
    public void SetOpacityValue(float opacityValueInPercents)
    {
        if (InvertedChange)
        {
            opacityValueInPercents = OpacityController.MaxOpacityValue - opacityValueInPercents;
        }
        OpacityChangingStrategy.SetOpacity(ChangingMaterial, opacityValueInPercents * 0.01f);
    }

    /// <summary>
    /// Установить режим прозрачного материала
    /// </summary>
    public void SetFadeMode()
    {
        OpacityChangingStrategy.SetFadeMode(ChangingMaterial);
    }

    /// <summary>
    /// Установить режим непрозрачного материала
    /// </summary>
    public void SetOpaqueMode()
    {
        OpacityChangingStrategy.SetOpaqueMode(ChangingMaterial);
    }
}
