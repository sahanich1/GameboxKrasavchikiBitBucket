using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ���������� ����� �� ������.
/// ��������� �� ������� �������� ���� � ������� ������.
/// ����������� �� ������ �������� ���������.
/// ������� ������� �� ������� ����������� PlayerController (��� ������� ���
/// �������� � ��������) � WeaponController (��� ���������� �������)
/// </summary>
[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(WeaponController))]
public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    [Tooltip("������ ������� ������ �������, � ������� ���������� ��������������� �� ������")]
    private float autoTargetingRadius = 2;

    [SerializeField]
    [Tooltip("������ HP ������")]
    private DamageableObject playerHealthObject = null;

    /// <summary>
    /// ������� ����������� �������� ������
    /// </summary>
    private Vector3 moveDirection;

    /// <summary>
    /// ���������� ������
    /// </summary>
    private PlayerController playerController;

    /// <summary>
    /// ���������� ������ ������
    /// </summary>
    private WeaponController weaponController = null;
    
    /// <summary>
    /// ������ �� ������ �����
    /// </summary>
    private bool isPushingAttackButton = false;

    /// <summary>
    /// ������ �� ������� ������
    /// </summary>
    private Camera mainCamera = null;

    /// <summary>
    /// ������� ������� ���� �� ��������� XZ, � ������� ��������� �����
    /// </summary>
    private Vector3 mouseGroundPosition;

    /// <summary>
    /// ���������, � ������� ��������� �����
    /// </summary>
    private Plane playerMovingPlane;

    /// <summary>
    /// ������� ���� � ������� ��������������� ������ �������
    /// </summary>
    private IDamageable currentTarget = null;

    /// <summary>
    /// ������ ������� ������ �������, � ������� ���������� ��������������� �� ������
    /// </summary>
    public float AutoTargetingRadius => autoTargetingRadius;

    /// <summary>
    /// �������� ������� ���� ������
    /// </summary>
    /// <returns></returns>
    public IDamageable GetTarget()
    {
        return currentTarget;
    }

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
        weaponController = GetComponent<WeaponController>();
        mainCamera = Camera.main;
        playerMovingPlane = new Plane(Vector3.up, transform.position + 
            new Vector3(0, weaponController.AttackPoint.position.y, 0));
    }

    private void Update()
    {
        // ���� ����� ����, �� ��������� ����� � ������ �� ��������� �� �� ����� ������� ������
        if (!playerHealthObject.IsAlive())
        {
            if (weaponController.IsAttacking())
            {
                weaponController.InterruptAttack();
            }          
            return;
        }

        float horizontalInput = Input.GetAxis(ControlConstants.HorizontalMove);
        float verticalInput = Input.GetAxis(ControlConstants.VerticalMove);
        moveDirection = new Vector3(-horizontalInput, 0, -verticalInput).normalized;

        mouseGroundPosition = GetMouseGroundPosition(Input.mousePosition);

        currentTarget = FindNearestTargetInVicinity(mouseGroundPosition, autoTargetingRadius);
        
        if (Input.GetButtonDown(ControlConstants.AttackButton))
        {
            weaponController.SetTarget(currentTarget);
            isPushingAttackButton = true;
        }

        if (Input.GetButtonUp(ControlConstants.AttackButton))
        {
            weaponController.InterruptAttack();
            isPushingAttackButton = false;
        }

        if (isPushingAttackButton)
        {
            // ���� ������ ����� ������, ������ ������� ��������� ������ ���� �� ��� ������,
            // ���� ������� �� ���������� ����� �� ����������, ����� ����� ����� ���� ������ �����
            weaponController.Attack();
        }
    }

    private void FixedUpdate()
    {
        if (!playerHealthObject.IsAlive())
        {
            return;
        }

        playerController.Move(moveDirection);

        // ��� ����� �������������� ����, ��� ��������� ����.
        // ���� ���� ���, ������� �� ������.
        IDamageable target = weaponController.GetTarget();
        Vector3 lookAtPoint = target != null ? target.Transform.position : mouseGroundPosition;
        playerController.RotateSmooth(lookAtPoint);
    }

    /// <summary>
    /// �������� �������� ������� ������� ���� �� ��������� XZ, � ������� ��������� �����
    /// </summary>
    /// <param name="mousePosition">�������� ���������� �������</param>
    /// <returns></returns>
    private Vector3 GetMouseGroundPosition(Vector3 mousePosition)
    {
        Ray cameraToCursorRay = mainCamera.ScreenPointToRay(mousePosition);
        if (playerMovingPlane.Raycast(cameraToCursorRay, out float hitDistance))
        {
            return cameraToCursorRay.GetPoint(hitDistance);
        }
        return Vector3.zero;
    }

    /// <summary>
    /// ����� �����, ���������� � ��������� �������
    /// </summary>
    /// <param name="point">����� �����������, � ������� �������������� �����</param>
    /// <param name="radius">������ �����������</param>
    /// <returns></returns>
    private IDamageable FindNearestTargetInVicinity(Vector3 point, float radius)
    {
        Vector3 topPoint = point;
        topPoint.y = VectorFunc.MaxRaycastDistance;

        RaycastHit[] hitInfo = Physics.SphereCastAll(topPoint, radius, Vector3.down,
            VectorFunc.MaxRaycastDistance, GameManager.Instance.EnemyLayers, QueryTriggerInteraction.Ignore);

        if (hitInfo.Length == 0)
        {
            return null;
        }
        
        Transform minDistTransform = hitInfo[0].transform;
        float minSqrDistToTarget = Vector3.SqrMagnitude(minDistTransform.position - point);

        // ���� ����� � ����������� ����� ���������, ����� ��, ������� ����� ����� � ������ �����������
        for (int i = 1; i < hitInfo.Length; i++)
        {
            float currentSqrDist = Vector3.SqrMagnitude(hitInfo[i].transform.position - point);
            if (currentSqrDist < minSqrDistToTarget)
            {
                minSqrDistToTarget = currentSqrDist;
                minDistTransform = hitInfo[i].transform;
            }
        }

        return minDistTransform.GetComponent<IDamageable>();
    }

}
