using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ���� ������ ������� ����������.
/// ������ ��� ����������� ������� ������.
/// ����������� ���� �� ������ �����, ���� ��� ��������� �������� ������ �����.
/// </summary>
public class FieldOfView : MonoBehaviour, ISaveable
{
    /// <summary>
    /// ��������� ����������� ����
    /// </summary>
    public enum TargetDetectingState
    {        
        /// <summary>
        /// ���� ����������
        /// </summary>
        Detected,
        /// <summary>
        /// ���� �� ����������
        /// </summary>
        NotDetected,
        /// <summary>
        /// ������� � ���� ������ ��������
        /// </summary>
        AllyIsUnderAttack
    }
    
    /// <summary>
    /// ����� �������� ���� � ���� ������
    /// </summary>
    public enum CheckTargetMode
    {
        /// <summary>
        /// ��������� ������ � ������� ���� ������
        /// </summary>
        Fov,
        /// <summary>
        /// ��������� � ������� ���� ������ � � ������� ���������������� 
        /// </summary>
        FovAndSensitivity,
    }

    /// <summary>
    /// �������� �������� � �������� (����� �� ������ ���� ���������) 
    /// </summary>
    const float checkTargetInFovDelay = 0.1f;

    [SerializeField]
    [Tooltip("����� ������ ���� ������")]
    private Transform pointOfSight = null;

    [SerializeField]
    [Tooltip("���� ���� ������")]
    private float angle = 0;

    [SerializeField]
    [Tooltip("������ ���� ������")]
    private float distance = 0;

    [SerializeField]
    [Tooltip("������ ����������������")]
    private float sensitivityDistance = 2;

    [SerializeField]
    [Tooltip("�������� ����������� ���� � ��������� ���������")]
    private float idleDetectionDelay = 1.5f;

    [SerializeField]
    [Tooltip("�������� ����������� ���� � ��������� ������� " +
        "(����� ���� ������ ��� ������� ������ �� ����)")]
    private float alertDetectionDelay = 0;

    [SerializeField]
    [Tooltip("�������� ����������� ��������� �� ��������")]
    private float attackedAllyDetectionDelay = 0;

    /// <summary>
    /// ������ ������, �������� ���� �������� ����������
    /// </summary>
    private IDamageable target = null;

    /// <summary>
    /// ������� �������� ����������� ����
    /// </summary>
    private float currentDetectionDelay = 0;

    /// <summary>
    /// ������� �������� ������� ���� ������ (��� �����������) 
    /// </summary>
    private float sqrDistance = 0;

    /// <summary>
    /// ������� �������� ������� ���������������� (��� �����������) 
    /// </summary>
    private float sqrSensitivityDistance = 0;

    /// <summary>
    /// �������� �������� ���� ���� ������ (��� �����������) 
    /// </summary>
    private float halfAngle = 0;

    /// <summary>
    /// ������� ��������� ����������� ����
    /// </summary>
    private TargetDetectingState targetDetectingState = TargetDetectingState.NotDetected;

    /// <summary>
    /// ������ ���� ������
    /// </summary>
    public float Distance => distance;

    /// <summary>
    /// ������ ����������������
    /// </summary>
    public float SensitivityDistance => sensitivityDistance;

    /// <summary>
    /// ���� ���� ������
    /// </summary>
    public float Angle => angle;

    /// <summary>
    /// ����� ������ ���� ������
    /// </summary>
    public Transform PointOfSight => pointOfSight;

    /// <summary>
    /// ������� ��������� ����������� ����
    /// </summary>
    public TargetDetectingState DetectingState => targetDetectingState;

    /// <summary>
    /// ���������� ����
    /// </summary>
    public Vector3 TargetPosition => target.Transform.position;

    /// <summary>
    /// ���������, ��������� �� ����� � ������� ���� ������
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public bool IsPointInFovRadius(Vector3 point)
    {
        float sqrDistToPoint = VectorFunc.GetSqrDistanceXZ(PointOfSight.position, point);
        return sqrDistToPoint <= sqrDistance;
    }

    /// <summary>
    /// ���������� �������� ����������� � ��������� ���������
    /// </summary>
    public void SetIdleDetectionDelay()
    {
        currentDetectionDelay = idleDetectionDelay;
    }

    /// <summary>
    /// ���������� �������� ����������� � ��������� �������
    /// </summary>
    public void SetAlertDetectionDelay()
    {
        currentDetectionDelay = alertDetectionDelay;
    }

    /// <summary>
    /// ��������/��������� ����������� ���� ������
    /// </summary>
    /// <param name="enabled"></param>
    public void SetEnabled(bool enabled)
    {
        if (FieldOfViewManager.Instance == null)
        {
            return;
        }

        if (enabled)
        {
            FieldOfViewManager.Instance.AddFieldOfView(this);
        }
        else
        {
            FieldOfViewManager.Instance.RemoveFieldOfView(this);
        }
    }

    private void Awake()
    {
        sqrDistance = distance * distance;
        sqrSensitivityDistance = sensitivityDistance * sensitivityDistance;
        halfAngle = angle / 2;
        pointOfSight.forward = new Vector3(pointOfSight.forward.x, 0, pointOfSight.forward.z).normalized;
    }

    private void Start()
    {
        target = GameManager.Instance.Player;
        SetEnabled(true);
        StartSearching();
    }

    private void OnDestroy()
    {
        SetEnabled(false);
        StopAllCoroutines();
    }

    /// <summary>
    /// ��������, ����������� ���������� ����� ����.
    /// ������ ����� ����� ������� ���������, ��� ����� ������������ ��������, 
    /// �.�. �� ��������� AllyIsUnderAttack �� ����� ������� � ��������� TargetDetectingState.Detected,
    /// �� �� ��������.
    /// </summary>
    /// <returns></returns>
    private IEnumerator TargetSearching()
    {      
        while (true)
        {
            bool targetInFOV = CheckTargetInFov(target.Transform.position, CheckTargetMode.Fov);
            if (targetInFOV)
            {
                if (targetDetectingState != TargetDetectingState.Detected)
                {
                    yield return new WaitForSeconds(currentDetectionDelay);
                    targetInFOV = CheckTargetInFov(target.Transform.position, CheckTargetMode.Fov);
                    if (targetInFOV)
                    {
                        SetTargetDetectingState(TargetDetectingState.Detected);
                    }
                }
            }
            else if (targetDetectingState == TargetDetectingState.Detected)
            {
                SetTargetDetectingState(TargetDetectingState.NotDetected);
            }

            yield return new WaitForSeconds(checkTargetInFovDelay);
        }
    }

    /// <summary>
    /// ��������, ����������� ����� ��������, �������� ����� ������
    /// </summary>
    /// <returns></returns>
    private IEnumerator AllyToHelpSearching()
    {
        while (true)
        {
            // ���� ���� ��� ����������, ��� ������ ��������� ����� ������������ ��������
            if (targetDetectingState == TargetDetectingState.Detected)
            {
                yield return null;
                continue;
            }

            IDamageable ally = FindAllyToHelp();
            if (ally != null)
            {
                if (targetDetectingState == TargetDetectingState.NotDetected)
                {
                    yield return new WaitForSeconds(attackedAllyDetectionDelay);
                    ally = FindAllyToHelp();
                    if (ally != null && targetDetectingState != TargetDetectingState.Detected)
                    {
                        SetTargetDetectingState(TargetDetectingState.AllyIsUnderAttack);
                    }
                }
            }
            else if (targetDetectingState == TargetDetectingState.AllyIsUnderAttack)
            {
                SetTargetDetectingState(TargetDetectingState.NotDetected);
            }

            yield return new WaitForSeconds(checkTargetInFovDelay);
        }
    }

    /// <summary>
    /// ������ ����� � ���� ������
    /// </summary>
    private void StartSearching()
    {
        StartCoroutine(TargetSearching());
        StartCoroutine(AllyToHelpSearching());
    }

    /// <summary>
    /// ���������� ��������� ����������� ����
    /// </summary>
    /// <param name="state"></param>
    private void SetTargetDetectingState(TargetDetectingState state)
    {
        targetDetectingState = state;
    }

    /// <summary>
    /// ���������, ��������� �� ������ � �������� ���������
    /// </summary>
    /// <param name="targetPosition">���������� �������</param>
    /// <param name="checkMode">����� ��������</param>
    /// <returns></returns>
    private bool CheckTargetInFov(Vector3 targetPosition, CheckTargetMode checkMode)
    {
        Vector3 vectorToTarget = targetPosition - pointOfSight.position;
        vectorToTarget.y = 0;
        float sqrDistanceToTarget = Vector3.SqrMagnitude(vectorToTarget);

        // ��������, �� ��������� �� ���� � ������� ����������������.
        // ���� ��� ��� ���� ���������� ����� �������� ������ � ������� ���� ������,
        // �� ����� ������� �������������� ��������
        if (checkMode != CheckTargetMode.FovAndSensitivity 
            || sqrDistanceToTarget > sqrSensitivityDistance)
        {
            // ��������, �� ��������� �� ���� �� ��������� ��������� ���������
            if (sqrDistanceToTarget > sqrDistance)
            {
                return false;
            }

            // ��������, �������� �� ���� � ������� ���� ������
            if (Vector3.Angle(pointOfSight.forward, vectorToTarget) > halfAngle)
            {
                return false;
            }
        }

        // ���� ����������� �� ���� � ���� ���, �� ���� �����
        RaycastHit hitInfo = FieldOfViewManager.GetFovRaycastHit(pointOfSight.position, vectorToTarget,
            Mathf.Sqrt(sqrDistanceToTarget));
        return hitInfo.collider == null;
    }

    /// <summary>
    /// ���������� ����� ������������ �������� � ���� ������
    /// </summary>
    /// <returns></returns>
    private IDamageable FindAllyUnderAttack()
    {
        WeaponController playerWeapon = GameManager.Instance.PlayerWeapon;
        if (playerWeapon == null)
        {
            return null;
        }

        IDamageable enemyUnderAttack = playerWeapon.GetTarget();
        if (enemyUnderAttack == null || enemyUnderAttack.Transform == transform ||
            !playerWeapon.IsHitPerforming())
        {
            return null;
        }

        if (CheckTargetInFov(enemyUnderAttack.Transform.position,
            CheckTargetMode.FovAndSensitivity))
        {
            return enemyUnderAttack;
        }

        return null;
    }

    /// <summary>
    /// ����� ��������, ������� ������� ������
    /// </summary>
    /// <returns></returns>
    private IDamageable FindAttackingAlly()
    {
        IDamageable allyToHelp = null;

        foreach (var enemy in EnemyManager.Instance.AttackingEnemies)
        {
            if (CheckTargetInFov(enemy.Transform.position, CheckTargetMode.FovAndSensitivity))
            {
                allyToHelp = enemy;
                break;
            }
        }

        return allyToHelp;
    }

    /// <summary>
    /// ����� ��������, �������� ����� ������.
    /// ����� ���������� ������ IDamageable, ������� ��������� ��� ������ ������
    /// ��� ������� ��� ������� ������.
    /// </summary>
    /// <returns></returns>
    private IDamageable FindAllyToHelp()
    {
        // ������� �������, �� ������� �� ����-�� �����. 
        // ���� �������, �� ����� ���������� �����, ������� �������� ����� ������
        IDamageable allyToHelp = FindAllyUnderAttack();
        if (allyToHelp != null)
        {
            return allyToHelp;
        }

        // ���� ����� � ������ ������ ������ �� �������, �� ���������� ����� �����,
        // ������� ������� ������.
        return FindAttackingAlly();

    }

    /// <summary>
    /// ���������� ��������� ������� ���� ������ �� ���������
    /// </summary>    
    private void SetDefaultState()
    {
        SetTargetDetectingState(TargetDetectingState.NotDetected);
        StopAllCoroutines();
        Start();
    }

    /// <summary>
    /// ������ ��������� �������. 
    /// ������������ ��� �������������� ��������� ������� ��� ����������� �� ����������� �����.
    /// ������������ ����� ����� ���� ����������� ��� �������� �� �����,
    /// �� ������ �� ���������� ��� ������ ��� ����, ����� ��� ������ ���� � ����������� �����
    /// ������������ �������������� ��������� �������
    /// </summary>
    /// <param name="state"></param>
    public void SetState(object state)
    {
        SetDefaultState();
    }

    /// <summary>
    /// �������� ��������� �������.
    /// ������������ ����� ����� ���� ����������� ��� ���������� ������� � ����,
    /// ������ ��� �� ���������, ������� ���������� ������ ������ � �������� ���������
    /// </summary>
    /// <returns></returns>
    public object GetState()
    {
        return "";
    }
}
