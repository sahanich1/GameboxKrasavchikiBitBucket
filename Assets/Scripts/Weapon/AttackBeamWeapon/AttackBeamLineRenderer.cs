﻿using UnityEngine;

/// <summary>
/// Класс, использующий LineRenderer для визуального отображения атакующего луча
/// </summary>
public class AttackBeamLineRenderer : AttackBeamRenderer
{
    [SerializeField]
    [Tooltip("Компонент LineRenderer, используемый для отрисовки")]
    private LineRenderer lineRenderer;

    /// <summary>
    /// Установить начальную и конечную точку луча
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="endPoint"></param>
    public override void SetPosition(Vector3 startPoint, Vector3 endPoint)
    {
        lineRenderer.SetPosition(0, startPoint);
        lineRenderer.SetPosition(1, endPoint);
    }

    private void Awake()
    {
        lineRenderer.useWorldSpace = true;       
    }
}
