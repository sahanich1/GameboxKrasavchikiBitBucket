using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ���������� ������, ����������� �����
/// </summary>
public class BeamWeaponController : WeaponController
{
    [SerializeField]
    [Tooltip("��������� ���")]
    private AttackBeam attackBeam = null;

    /// <summary>
    /// ���������� ���� ����� �����
    /// </summary>
    /// <param name="target"></param>
    public override void SetTarget(IDamageable target)
    {
        attackBeam.SetTarget(target);
    }

    /// <summary>
    /// �������� ����
    /// </summary>
    /// <returns></returns>
    public override IDamageable GetTarget()
    {
        return attackBeam.GetTarget();
    }

    /// <summary>
    /// ��������� ����. � ������ ������ ���� ����� �������� �� �������
    /// ���� ��� �� ����� �������
    /// </summary>
    protected override void Hit()
    {
        isHitPerforming = true;
        StartCoroutine(PerformHit());
    }

    protected override void Awake()
    {
        base.Awake();
        attackBeam.Initialize(attackPoint, attackDistance);   
    }  

    /// <summary>
    /// �������� ���������� �����
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformHit()
    {
        attackBeam.StartAttack();
        while (true)
        {
            yield return null;
            if (isInterruptionRequested || !attackBeam.IsAttacking())
            {
                attackBeam.StopAttack();
                break;
            }
        }

        isHitPerforming = false;
    }
}
