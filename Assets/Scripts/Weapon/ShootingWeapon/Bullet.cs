using System.Collections;
using UnityEngine;

/// <summary>
/// ���������� ���������� ����
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    [SerializeField] 
    private float bulletStartSpeed;

    [SerializeField, Tooltip("������� �������� ����� ������� �� ��� �����")] 
    private int defaultDamageDevisor = 5;

    [SerializeField, Tooltip("����������� ����")] 
    private int minDamageValue = 10;

    [SerializeField, Tooltip("������������ ��� ����������� �����"), Range(0.1f, 0.9f)] 
    private float minAlpha = 0.1f;

    [SerializeField] private Transform bulletMesh;

    private Rigidbody bulletRigidbody;
    /// <summary>
    /// ���������� �� ������� �������� ����
    /// </summary>
    private float bulletLiveMaxDistance;
    /// <summary>
    /// ��������� ����� ����
    /// </summary>
    private Vector3 startPosition;
   
    private int currentDamageValue;

    private GameObject owner;

    private Transform targetTransform;

    /// <summary>
    /// �������� ����
    /// </summary>
    private Material material;
    

    public int DefaultDamageValue => defaultDamageDevisor;

    private void Awake()
    {
        bulletRigidbody = GetComponent<Rigidbody>();        

        currentDamageValue = defaultDamageDevisor;

        material = GetComponent<Renderer>().material;        
    }

    /// <summary>
    /// �������� ��������� ��������� ����
    /// </summary>
    /// <param name="owner">�������� ����</param> 
    /// <param name="startPosition">������ ������� ����</param>
    /// <param name="rotation">������� ��������� ����</param>
    /// <param name="velocity">�������� ��������� ����</param>
    public void SetBulletParameters(GameObject owner, Vector3 startPosition, Quaternion rotation, 
        Vector3 velocity, Transform targetBulletPoint, float damageMultiplier)
    {
        //�������� ����
        this.owner = owner;

        //��������� ������� ���� = ������� ��������� ����
        transform.position = startPosition;

        //Vector3 ���� ����. ������ ���������� � ����� �� ��� � ��������� �����
        targetTransform = targetBulletPoint;               

        //���������� ��������� ������� ��� ������������ ��������� ����������
        this.startPosition = transform.position;

        //������ ��������� �������� ��������
        transform.rotation = rotation;

        //������ ��������� �������� ��������
        bulletRigidbody.velocity = velocity;

        //��������� ������������ ���������� ������ ����
        this.bulletLiveMaxDistance = (startPosition - targetTransform.position).magnitude * 2;

        //���������� �������� ����� ����
        currentDamageValue = ((int)(ConciderOwnersHealth(owner, out _) / defaultDamageDevisor)) > minDamageValue         
            ? (int)(ConciderOwnersHealth(owner, out _) / defaultDamageDevisor)         
            : minDamageValue;

        //������������� ������������ ���� ������ �� ������ �������� �����
        SetMaterialAlphaByHealth(owner, material);
    }

    /// <summary>
    /// ���� � GameObject ���������� ��������� DamagableObject, �� ����� �������� �������� Health ����� �������
    /// </summary>
    /// <param name="owner"></param>
    /// <returns></returns>
    private float ConciderOwnersHealth(GameObject owner, out float maxHealth)
    {
        DamageableObject damageableOwner;

        if (owner != null)
        {
            if (owner.TryGetComponent<DamageableObject>(out damageableOwner))
            {
                maxHealth = damageableOwner.MaxHealth;
                return damageableOwner.Health;
            }                
            else
            {
                maxHealth = 1.0f;
                return 1.0f;
            }
        }        
        else
        {
            maxHealth = 1.0f;
            return 1.0f;
        }
    }

    /// <summary>
    /// ������������ ���� ������ �� �������� ����������� �������
    /// </summary>
    /// <param name="owner">����������� �� �������� ������</param>
    /// <param name="material">��������, ������������ �������� �������</param>
    private void SetMaterialAlphaByHealth(GameObject owner, Material material)
    {
        float maxHealth;
        float currentHealth;

        currentHealth = ConciderOwnersHealth(owner, out maxHealth);

        float targetAlpha = ((1 - minAlpha) * currentHealth / defaultDamageDevisor) /
            ((maxHealth / defaultDamageDevisor) - minDamageValue);

        material.color = new Color(material.color.r, material.color.g, material.color.b, targetAlpha);
    }

    /// <summary>
    /// �������� �������� ����
    /// </summary>
    public void BulletStartMoving()
    {
        StartCoroutine(BulletMoving());
    }
    
    private IEnumerator BulletMoving()
    {
        while (true)
        {
            // ���� ����� ������ ������ �������������
            Vector3 targetPosition = targetTransform.position;
            targetPosition.y = transform.position.y;

            transform.position = Vector3.MoveTowards(transform.position,
                targetPosition, bulletStartSpeed * 0.01f);
            yield return new WaitForFixedUpdate();
        }
    }

    private void Update()
    {
        BulletDistanceCounter();
    }

    private void OnTriggerEnter(Collider other)
    {
        // ��������, ����� �� ����������� �� ������������ � �����������
        if (!IsReactionOnCollisionNeeded(other))
        {
            return;
        }

        if (other.TryGetComponent(out IDamageable damageableObject))
        {
            damageableObject.ReceiveDamage(currentDamageValue);
            damageableObject.HitEffect();
        }
        
        DisableBullet();          
    }

    private void OnTriggerStay(Collider other)
    {
        // ��������� ����, ���� ��� ��� ��������� � �����������, ������� ���� ����������,
        // �� ����� ������������ �� ����� ������ ����� ����
        // (������ ������ ��������� �������� � ��� ����� ����, �� ���� ����� �������� ������
        // � ���������/������������ �����������, ��������, ����������)
        if (IsReactionOnCollisionNeeded(other))
        {
            DisableBullet();
        }
    }

    /// <summary>
    /// ���������, ����� �� ����������� �� ������������ � �����������
    /// </summary>
    /// <param name="other">��������� �������, � ������� ����������� ������������� �������</param>
    /// <returns></returns>
    private bool IsReactionOnCollisionNeeded(Collider other)
    {
        // �� ����� �����������, ���� ��������� ��������� � ����, � ��������� � � ���������� �����
        return !other.isTrigger &&
            (owner == null || (other.gameObject != owner && other.gameObject.layer != owner.layer));
    }

    private void BulletDistanceCounter()
    {
        if ((transform.position - startPosition).magnitude > bulletLiveMaxDistance) DisableBullet();
    }

    /// <summary>
    /// ����� ���������� ����
    /// </summary>
    /// <returns></returns>
    private void DisableBullet()
    {        
        this.gameObject.SetActive(false);
    }
}
