﻿using UnityEngine;

/// <summary>
/// Расширение функционала игровых объектов GameObject
/// </summary>
public static class GameObjectExtensions
{
    /// <summary>
    /// Проверить, содержится ли слой, назначенный на игровой объект, в наборе слоёв (LayerMask)
    /// </summary>
    /// <param name="gameObject">Игровой объект, слой которого проверяем</param>
    /// <param name="layerMask">Маска слоёв, в которой пытаемся найти слой gameObject</param>
    /// <returns></returns>
    public static bool IsLayerInLayerMask(this GameObject gameObject, LayerMask layerMask)
    {
        return layerMask == (layerMask | 1 << gameObject.layer);
    }

}
